#include "chicken.h"

std::random_device Chicken::rd;
std::default_random_engine Chicken::generator(rd());

Chicken::Chicken(std::string _name) : name(_name)
{
    state = idle;
    stopped = true;
}

Chicken::~Chicken()
{
    t.join();
}

void Chicken::laying()
{
    t = std::thread([this]() -> void {
        state = lay;
        stopped = false;
        while (!stopped)
        {
            std::uniform_int_distribution<int> distribution(2, 10);
            sleep(distribution(generator));
            eggs++;
        }
        
        state = idle;
    } );
    
}

void Chicken::stop()
{
    stopped = true;
}

std::string Chicken::getState()
{
    std::string ret_string;

    ret_string = "Name: " + name;

    switch (state)
    {
    case idle:
        ret_string += " State: Idle";
        break;
    case lay:
        ret_string += " State: Laying";
        break;
    }

    ret_string += " Eggs:" + std::to_string(eggs);
    
    return ret_string;
}