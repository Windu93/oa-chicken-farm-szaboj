#include <iostream>
#include <list>

#include "chicken.h"

using std::cout;
using std::cin;
using std::endl;

int main(int argc, char * argv[])
{
    bool quit = false;
    char option;
    std::list<Chicken*> chickens;
    std::string name;
    Chicken * new_chicken;
    
    while (!quit)
    {
        cout << "1. Create new chicken" << endl;
        cout << "2. Get chickens status" << endl;
        cout << "3. Stop chickens" << endl;
        cout << "4. Quit" << endl;

        cin >> option;

        switch (option)
        {
        case '1':
            cout << "Add chicken's name: ";
            
            cin >> name;

            new_chicken = new Chicken(name);
            new_chicken->laying();
            chickens.push_back(new_chicken);
            break;

        case '2':
            for (auto & chicken : chickens)
                cout << chicken->getState() << endl;
            break;

        case '3':
            for (auto & chicken : chickens)
                    chicken->stop();
            break;  

        case '4':
            quit = true;
            break;

        default:
            cout << "Invalid option number" << endl;
            break;
        }
    }

    for (auto & chicken : chickens) {
        chicken->stop();
        delete chicken;
    }
        

    return 0;
}