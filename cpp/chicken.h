#pragma once

#include <string>
#include <thread>
#include <mutex>
#include <atomic>
#include <unistd.h>
#include <random>

typedef enum State { lay, idle };

class Chicken
{
private:
    std::string name;
    int eggs;
    State state;
    std::atomic<bool> stopped;
    std::thread t;
    std::mutex mu;
    static std::random_device rd;
    static std::default_random_engine generator;
public:
    Chicken(std::string _name);
    ~Chicken();
    void laying();
    void stop();
    std::string getState();
};
