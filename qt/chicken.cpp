#include "chicken.h"

std::random_device Chicken::rd;
std::default_random_engine Chicken::generator(rd());

Chicken::Chicken(QString _name) : name(_name)
{
    state = idle;
    stopped = true;
}

Chicken::~Chicken()
{
}

void Chicken::run()
{
    state = lay;
    stopped = false;
    while (!stopped)
    {
        std::uniform_int_distribution<int> distribution(2, 10);
        sleep(distribution(generator));
        eggs++;
    }
    
    state = idle;
}

void Chicken::stop()
{
    stopped = true;
}

QString Chicken::getState()
{
    QString ret_string;

    ret_string = QStringLiteral("Name: ") + name;

    switch (state)
    {
    case idle:
        ret_string += QStringLiteral(" State: Idle");
        break;
    case lay:
        ret_string += QStringLiteral(" State: Laying");
        break;
    }

    ret_string += QStringLiteral(" Eggs:") + QString::number(eggs);
    
    return ret_string;
}