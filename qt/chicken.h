#pragma once

#include <QString>
#include <QThread>
#include <unistd.h>
#include <random>

typedef enum State { lay, idle };

class Chicken : public QThread
{
private:
    QString name;
    int eggs;
    State state;
    std::atomic<bool> stopped;
    static std::random_device rd;
    static std::default_random_engine generator; 
protected:
    void run() override;
public:
    Chicken(QString _name);
    ~Chicken();
    void stop();
    QString getState();
};
