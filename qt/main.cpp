#include <iostream>
#include <QCoreApplication>
#include <QList>
#include <QObject>

#include "chicken.h"

using std::cout;
using std::cin;
using std::endl;

int main(int argc, char * argv [])
{
    QCoreApplication app(argc, argv);

    bool quit = false;
    char option;
    QList<Chicken*> chickens;
    std::string name;
    Chicken * new_chicken;
    
    while (!quit)
    {
        cout << "1. Create new chicken" << endl;
        cout << "2. Get chickens status" << endl;
        cout << "3. Stop chickens" << endl;
        cout << "4. Quit" << endl;

        cin >> option;

        switch (option)
        {
        case '1':
            cout << "Add chicken's name: ";
            
            cin >> name;

            new_chicken = new Chicken(QString::fromStdString(name));
            QObject::connect(
                new_chicken, &QThread::finished,
                &app, &QCoreApplication::quit
            );
            new_chicken->start();
            chickens.push_back(new_chicken);
            break;

        case '2':
            foreach (auto & chicken, chickens)
                cout << chicken->getState().toStdString() << endl;
            break;

        case '3':
            foreach (auto & chicken, chickens)
                    chicken->stop();
            break;  

        case '4':
            quit = true;
            break;

        default:
            cout << "Invalid option number" << endl;
            break;
        }
    }

    foreach (auto & chicken, chickens) {
        chicken->stop();
        delete chicken;
    }

    return QCoreApplication::exec();
}